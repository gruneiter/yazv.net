const header = () => {
  const h = document.querySelector('.header__navigation');
  const hTopPos = h.getBoundingClientRect().top;
  const hHeight = h.offsetHeight;
  const pageBlock = document.querySelector('.page');
  window.addEventListener('scroll', () => {
    if (window.pageYOffset >= hTopPos && !h.classList.contains('header__navigation--fixed')) {
      h.classList.add('header__navigation--fixed');
      pageBlock.style.setProperty('--page-margin-top', `${hHeight}px`);
    } else if (window.pageYOffset < hTopPos) {
      h.classList.remove('header__navigation--fixed');
      pageBlock.style.setProperty('--page-margin-top', '0');
    }
  });
};

export default header;
