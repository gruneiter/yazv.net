export default function headerMobileSwitcher() {
  const hms = document.querySelector('.header-mobile-switcher');
  const nav = document.querySelector('.header-navigation');
  const header = document.querySelector('.header');
  window.addEventListener('DOMContentLoaded', () => {
    nav.style.setProperty('--top', `${header.offsetHeight}px`);
  });
  hms.addEventListener('click', (e) => {
    nav.classList.toggle('header-navigation--active');
    if (nav.classList.contains('header-navigation--active')) {
      e.target.classList.add('header-mobile-switcher--active');
      document.documentElement.classList.add('no-scroll');
    } else {
      e.target.classList.remove('header-mobile-switcher--active');
      document.documentElement.classList.remove('no-scroll');
    }
  });
}
