import FastAverageColor from 'fast-average-color';

const sliderWorks = () => {
  const works = Array.from(document.querySelectorAll('.slider-works__slide'));
  works.forEach((work) => {
    const img = Array.from(work.querySelectorAll('img'));
    const fac = new FastAverageColor();
    img.forEach((image) => {
      image.addEventListener('load', () => {
        fac.getColorAsync(image.currentSrc).then((color) => {
          const rgb = color.rgb.match(/\(([^)]+)\)/)[1];
          image.style.setProperty('background-color', `rgba(${rgb}, .25)`);
        });
      });
    });
  });
};

export default sliderWorks;
