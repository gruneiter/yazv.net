import IMask from 'imask';

class Form {
  constructor(item, validation, settings = {}) {
    this.form = item.form;
    this.methods = validation.methods || {};
    this.requiredMessage = validation.required || 'Это обязательное поле';
    this.handler = item.handler;
    this.onSubmit = item.onSubmit;

    this.validateFadeOutTime = settings.validateFadeOutTime || 600;
    this.validateAutoDissappear = settings.validateAutoDissappear || 5000;

    this.inputClassName = settings.inputClassName || 'form-input';
    this.successMessage = item.message || {
      success: {
        title: '',
        text: '',
      },
      error: {
        title: '',
        text: '',
      },
    };
    this.status = 'success';
  }

  validate() {
    const { elements } = this.form;

    const checkRequirement = (input) => {
      const { required } = input;
      let res;
      if (required) {
        if (input.type === 'checkbox') res = !input.checked ? 'required' : '';
        else res = !input.value ? this.requiredMessage : '';
      }
      return res;
    };

    const validateInput = (input) => {
      const { type } = input;
      const { name } = input;
      const required = checkRequirement(input);
      let result = required || '';

      Object.entries({ name, type }).forEach((item) => {
        const propertyType = item[0];
        const propertyName = item[1].toLowerCase();
        if (this.methods[propertyName] && this.methods[propertyName].type === propertyType) {
          if (required) {
            result = this.methods[propertyName].required
              ? this.methods[propertyName].required
              : required;
          } else if (this.methods[propertyName].rules) {
            this.methods[propertyName].rules.forEach((rule) => {
              result = rule(input) === ''
                ? result
                : `${result}<span>${rule(input)}</span>`;
            });
          }
        }
      });
      return result;
    };

    const validationResults = [];
    Array.from(elements)
      .forEach((element) => {
        if (element.classList.contains(`${this.inputClassName}__field--clone`)) return;
        const invalidClass = `${this.inputClassName}--invalid`;
        const parent = element.parentNode;
        parent.classList.remove(invalidClass);

        const oldMessage = parent.querySelector('.form-input__message');
        if (oldMessage) oldMessage.remove();

        const result = validateInput(element);
        if (result) {
          const message = document.createElement('div');
          const messageClassName = `${this.inputClassName}__message`;
          const removeMessage = () => {
            message.classList.add(`${messageClassName}--disappear`);
            setTimeout(() => message.remove(), this.validateFadeOutTime);
          };
          message.className = messageClassName;
          validationResults.push(result);
          setTimeout(() => {
            parent.classList.add(invalidClass);
          }, 10);
          message.innerHTML = result;
          parent.append(message);
          element.addEventListener('input', (e) => {
            const res = validateInput(e.target);
            if (!res) e.target.parentNode.classList.remove(invalidClass);
          });
          element.addEventListener('focus', () => removeMessage());
          if (this.validateAutoDissappear > 0) {
            setTimeout(() => removeMessage(), this.validateAutoDissappear);
          }
        }
      });
    return validationResults;
  }

  showMessage() {
    const message = document.createElement('div');
    message.classList.add('form__message');
    message.innerHTML = `
      <div class="form__message-inner">
        <h2>${this.successMessage[this.status].title}</h2>
        <p>${this.successMessage[this.status].text}</p>
      </div>
      <a href="#" class="close-button form__message-close"></a>
    `;
    message.addEventListener('click', (event) => {
      if (event.target.classList.contains('form__message-close')) {
        event.preventDefault();
        message.classList.add('form__message--closing');
        setTimeout(() => message.remove(), 200);
      }
    });
    this.form.append(message);
  }

  submit() {
    this.form.classList.add('form--sending');
    fetch(this.handler, {
      method: 'POST',
      body: new FormData(this.form),
    })
      .then((response) => response.json())
      .then((result) => {
        this.status = result.result || this.status;
        if (this.status === 'error') {
          this.successMessage.error.text = result.message || this.successMessage.error.text;
          throw Error;
        }
      })
      .then(() => setTimeout(() => {
        if (this.onSubmit) this.onSubmit();
        this.form.classList.remove('form--sending');
        this.showMessage();
        this.form.reset();
      }, 400))
      .catch(() => setTimeout(() => {
        this.form.classList.remove('form--sending');
        this.status = 'error';
        this.showMessage();
      }, 400));
  }

  init() {
    this.form.setAttribute('novalidate', '');
    const tel = this.form.querySelectorAll('.form-input__field[type="tel"]');
    Array.from(tel).forEach((t) => IMask(t, { mask: '+7 (000) 000-00-00' }));
    this.form.addEventListener('submit', (event) => {
      event.preventDefault();
      const validationResult = this.validate();
      if (!validationResult.find((i) => (i))) {
        this.submit();
      }
    });
  }
}

export default Form;
