const appointmentForm = () => {
  const af = Array.from(document.querySelectorAll('.appointment-form'));
  if (af) {
    af.forEach((item) => {
      item.addEventListener('click', (e) => {
        if (e.target.classList.contains('appointment-form') || e.target.classList.contains('appointment-form__close')) {
          e.preventDefault();
          item.classList.remove('appointment-form--active');
        }
      });
    });
  }
};

const appointmentButton = () => {
  const buttons = Array.from(document.querySelectorAll('.appointment-button'));
  if (buttons) {
    buttons.forEach((btn) => {
      btn.addEventListener('click', (e) => {
        e.preventDefault();
        const af = document.getElementById(btn.href.substr(btn.href.indexOf('#') + 1));
        const form = af.querySelector('form');
        af.classList.add('appointment-form--active');
        const dataset = Object.keys(btn.dataset);
        dataset.forEach((d) => {
          if (form.elements[d]) form.elements[d].value = btn.dataset[d];
        });
      });
    });
  }
};

const appointment = () => {
  appointmentForm();
  appointmentButton();
};

export default appointment;
