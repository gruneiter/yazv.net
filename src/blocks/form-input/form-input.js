const textarea = () => {
  const ta = Array.from(document.querySelectorAll('.form-input--textarea'));
  if (ta) {
    ta.forEach((item) => {
      const input = item.querySelector('textarea');
      const inputClone = document.createElement('textarea');
      inputClone.classList.add(input.className, `${input.className}--clone`);
      input.parentNode.append(inputClone);
      input.addEventListener('input', () => {
        inputClone.value = input.value;
        input.style.setProperty('height', `${inputClone.scrollHeight + 1}px`);
      });
    });
  }
};

const labels = () => {
  const inputs = Array.from(document.querySelectorAll('.form-input'));

  if (inputs) {
    inputs.forEach((item) => {
      const input = item.querySelector('.form-input__field');
      input.addEventListener('input', () => {
        if (input.value) {
          item.classList.add('form-input--fill');
        } else {
          item.classList.remove('form-input--fill');
        }
      });
      input.form.addEventListener('reset', () => {
        item.classList.remove('form-input--fill');
      });
    });
  }
};

const formInput = () => {
  textarea();
  labels();
};

export default formInput;
